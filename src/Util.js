'use strict';

/**
 *  Converts a word ('yes','no') to boolean representation.
 *  It is canse-insensitive
 *
 * @param {String} word
 * @returns Boolean
 */
function convertWordToBool(word) {
    if (word && typeof(word) === 'string') {
        if (word.trim().toLowerCase() === 'yes') {
            return true;
        }else{
            if (word.trim().toLowerCase() === 'no') {
                return false;
            }else{
                throw new Error(word + ' is not a valid boolean word');
            }
        }
    }

    throw new Error('String word is expected');
}

module.exports.convertWordToBool = convertWordToBool;